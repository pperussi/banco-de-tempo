# Banco de Tempo

Projeto inicial de uma aplicação para um Banco de Tempo.

## Setup do projeto

Para rodar o projeto local usando Docker:

```sh
docker-compose up 
```

```sh
docker exec time_bank rails db:create
docker exec time_bank rails db:migrate
```

## Protótipo navegável

Figma link:
https://www.figma.com/file/zSZBy1ZD67hj3sOSHep2Q0/Banco-de-Tempo?node-id=0%3A1

## Projeto no Heroku

https://banco-de-tempo.herokuapp.com/

Usuário administrador para teste:

```
email: admin@email.com, password: admin123
```

## Vídeo de apresentação das funcionalidades

https://www.loom.com/share/bb6e6e6e7e1e45a6b65376384736d89d
