# frozen_string_literal: true

require "rails_helper"
include Dry::Monads[:result]

describe ExecuteTransaction do
  subject { described_class.new(transaction) }

  let(:user) { create(:user) }
  let(:another_user) { create(:user) }
  let(:hours) { 2 }
  let(:transaction) { create(:transaction, provider: user, receiver: another_user, hours: hours) }

  describe "success" do
    before { subject.call }

    context "when process transaction between users" do
      it { expect(transaction.status).to eq("aprovada") }
      it { expect(user.hours).to eq(2) }
    end

    context "when process transaction with entity" do
      let(:another_user) { create(:user, :entity) }

      it { expect(transaction.status).to eq("aprovada") }
      it { expect(another_user.hours).to eq(2) }
    end
  end

  describe "failure" do
    context "when can not create transaction" do
      before { allow(ActiveRecord::Base).to receive(:transaction).and_raise(StandardError) }

      it { expect(subject.call).to eq(Failure("Não foi possível requisitar talento, tente novamente.")) }
    end

    context "when balance is not enough" do
      let(:provider) { create(:user, hours: 3) }
      let(:hours) { 5 }

      it { expect(subject.call).to eq(Failure("O saldo do solicitante não é suficiente para a troca.")) }
    end
  end
end
