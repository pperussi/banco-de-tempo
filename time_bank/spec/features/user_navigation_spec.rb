# frozen_string_literal: true

require "rails_helper"

describe "user navigation", type: :feature do
  scenario "cant view unlogged buttons" do
    user = create(:user)

    login_as(user)
    visit root_path

    expect(page).not_to have_link("Entrar")
    expect(page).not_to have_link("Cadastrar conta")
  end

  scenario "can view exit button" do
    user = create(:user)

    login_as(user)
    visit root_path

    expect(page).to have_button("Sair")
  end

  scenario "authenticated user is redirect to initial users page" do
    user = create(:user)

    login_as(user)
    visit root_path

    expect(current_path).to eq(user_path(user))
  end
end
