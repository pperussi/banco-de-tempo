# frozen_string_literal: true

require "rails_helper"

describe "User see a list of talents", type: :feature do
  scenario "successfuly" do
    user = create(:user)
    5.times do
      create(:talent)
    end

    login_as(user)
    visit root_path

    click_on "Buscar Talentos"

    expect(page).to have_content(Talent.last.name)
  end
end
