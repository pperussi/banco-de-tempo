# frozen_string_literal: true

require "rails_helper"

describe "User add talents to profile", type: :feature do
  scenario "add new talent successfuly" do
    user = create(:user)

    login_as user
    visit new_talent_path

    fill_in "Nome", with: "Fazer bolo"
    fill_in "Descrição", with: "Faço um bolo de fubá delicioso <3"
    select "cuidado", from: "Categoria"
    click_on "Salvar"

    expect(page).to have_content("Fazer bolo")
  end
end
