# frozen_string_literal: true

require "rails_helper"

feature "new registration", type: :feature do
  scenario "view form" do
    visit root_path

    click_on "Cadastrar conta"
    fill_in "Email", with: "fulano@email.com"
    fill_in "Senha", with: "123456"
    fill_in "Confirme a senha", with: "123456"
    fill_in "Nome", with: "Fulano de tal"
    fill_in "Descrição", with: "Etc e tal"
    click_on "Cadastrar"

    expect(page).to have_content("Fulano de tal")
  end
end
