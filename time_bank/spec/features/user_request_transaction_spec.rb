# frozen_string_literal: true

require "rails_helper"

describe "User add talent", type: :feature do
  scenario "create transaction successfuly" do
    user = create(:user)
    another_user = create(:user)
    create(:talent, user: another_user)

    login_as user
    visit profile_path(profile_id: another_user.id)

    click_on "Solicitar Horas"
    fill_in "Horas", with: "2"
    click_on "Solicitar"

    visit transactions_path
    click_on "Enviadas"

    expect(page).to have_content(Transaction.last.receiver.name)
  end
end
