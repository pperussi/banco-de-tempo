# frozen_string_literal: true

require "rails_helper"

describe "User have a page to login/logout", type: :feature do
  scenario "login successfuly" do
    user = create(:user)

    visit root_path
    click_on "Entrar"
    fill_in "Email", with: user.email
    fill_in "Senha", with: user.password
    click_on "Enviar"

    expect(page).to have_button("Sair")
  end

  scenario "logout successfuly" do
    user = create(:user)

    login_as(user)
    visit root_path

    click_on "Sair"

    expect(page).to have_link("Entrar")
  end
end
