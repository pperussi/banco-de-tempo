# frozen_string_literal: true

require "rails_helper"

describe "User send message", type: :feature do
  scenario "create transaction successfuly" do
    user = create(:user)
    another_user = create(:user)

    login_as user
    visit profile_path(profile_id: another_user.id)

    fill_in "Mensagem", with: "Olá, bom dia!"
    click_on "Enviar"

    login_as another_user
    visit messages_path

    expect(page).to have_content(user.name)
  end
end
