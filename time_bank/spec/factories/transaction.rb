# frozen_string_literal: true

require "ffaker"

FactoryBot.define do
  factory :transaction do
    provider { create(:user) }
    receiver { create(:user) }
    talent { FFaker::JobBR.title }
    hours { rand(1..100) }
  end
end
