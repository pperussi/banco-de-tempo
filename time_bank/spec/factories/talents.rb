# frozen_string_literal: true

require "ffaker"

FactoryBot.define do
  factory :talent do
    user { create(:user) }
    name { FFaker::JobBR.title }
    description { FFaker::LoremBR.paragraph }
    category { Talent.categories.to_a.sample.first }
  end
end
