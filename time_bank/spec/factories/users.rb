# frozen_string_literal: true

require "ffaker"

FactoryBot.define do
  factory :user do
    name { FFaker::NameBR.name }
    email { FFaker::Internet.email }
    description { FFaker::LoremBR.paragraph }
    password { "123456" }

    trait :entity do
      name { FFaker::Company.name }
      entity { true }
    end

    trait :admin do
      admin { true }
    end
  end
end
