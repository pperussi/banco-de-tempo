# frozen_string_literal: true

class MessagesController < ApplicationController
  before_action :authenticate_user!

  def index
    @messages_received = Message.where(receiver: current_user)
    @messages_sent = Message.where(sender: current_user)
  end

  def new
    @message = Message.new
  end

  def create
    receiver = User.find(message_params[:receiver_id])

    @message = Message.new(message: message_params[:message])
    @message.receiver = receiver
    @message.sender = current_user

    if @message.save
      redirect_to profile_path(profile_id: receiver.id)
    else
      flash[:alert] = "Não foi possível salvar a mensagem, tente novamente."
      redirect_to profile_path(profile_id: receiver.id)
    end
  end

  private
    def message_params
      params.require(:message).permit(:receiver_id, :message)
    end
end
