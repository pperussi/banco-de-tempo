# frozen_string_literal: true

class TransactionsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_transaction, only: [:deny, :accept, :destroy]

  def index
    @my_transactions = Transaction.where(provider: current_user)
    @request_transactions = Transaction.where(receiver: current_user)
  end

  def new
    @transaction = Transaction.new
  end

  def create
    return hour_error if transaction_params[:hours].to_i <= 0

    receiver = User.find(transaction_params[:receiver_id])

    transaction = Transaction.new(
      talent: transaction_params[:talent],
      hours: transaction_params[:hours],
    )
    transaction.receiver = receiver
    transaction.provider = current_user

    if transaction.save
      redirect_to profile_path(profile_id: receiver.id)
    else
      flash[:alert] = "Não foi possível requisitar talento, tente novamente."
      redirect_to profile_path(profile_id: receiver.id)
    end
  end

  def deny
    if @transaction.update(status: :recusada)
      redirect_to transactions_path
    else
      flash[:alert] = "Não foi possível atualizar transação, tente novamente."
      redirect_to transactions_path
    end
  end

  def accept
    result = ExecuteTransaction.new(@transaction).call

    if result.success?
      redirect_to transactions_path
    else
      flash[:alert] = result.failure
      redirect_to transactions_path
    end
  end

  def destroy
    @transaction.destroy
    redirect_to transactions_path
  end

  private
    def set_transaction
      @transaction = Transaction.find(params[:id])
    end

    def transaction_params
      params.require(:transaction).permit(:talent, :hours, :receiver_id)
    end

    def hour_error
      flash[:alert] = "A quantidade mínima para requisitar um talento é uma hora."
      redirect_to profile_path(profile_id: transaction_params[:receiver_id])
    end
end
