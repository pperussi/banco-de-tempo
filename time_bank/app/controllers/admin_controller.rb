# frozen_string_literal: true

class AdminController < ApplicationController
  before_action :authenticate_user!
  before_action :verify_admin

  def dashboard
    @users_count = User.not_entities.count
    @entities_count = User.entities.count

    @users = User.not_entities
    @entities = User.entities
    @information = Information.all
  end

  def bank_hours
    @bank = User.find_by(name: "Banco")

    @my_transactions = Transaction.where(provider: @bank)
    @request_transactions = Transaction.where(receiver: @bank)
  end

  def users
    @users = User.not_entities
  end

  def update_user
    user = User.find(params[:format])

    if user.update(status: params[:status])
      redirect_to admin_dashboard_path
    else
      flash[:alert] = "Não foi possível atualizar transação, tente novamente."
      redirect_to admin_dashboard_path
    end
  end

  def entities
    @entities = User.entities
  end

  def update_entity
    user = User.find(params[:format])

    if user.update(status: params[:status])
      redirect_to admin_dashboard_path
    else
      flash[:alert] = "Não foi possível atualizar transação, tente novamente."
      redirect_to admin_dashboard_path
    end
  end

  def information
    @information = Information.all
  end

  private
    def verify_admin
      return if current_user.admin?

      redirect_to user_path(current_user)
    end
end
