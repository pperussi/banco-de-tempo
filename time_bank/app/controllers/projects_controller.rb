# frozen_string_literal: true

class ProjectsController < ApplicationController
  before_action :authenticate_user!
  before_action :verify_entity_user, only: [:new, :edit, :destroy]
  before_action :set_project, only: [:edit, :update, :destroy]

  def index
    @projects = Project.all
  end

  def entity
    @projects = current_user.projects
  end

  def new
    @project = Project.new
  end

  def create
    @project = Project.new(project_params)
    @project.user = current_user

    if @project.save
      redirect_to user_path(current_user)
    else
      flash[:alert] = "Não foi possível atualizar o projeto, tente novamente."
      render :new
    end
  end

  def edit
  end

  def update
    if @project.update(project_params)
      redirect_to user_path(current_user)
    else
      flash[:alert] = "Não foi possível atualizar o projeto, tente novamente."
      redirect_to user_path(current_user)
    end
  end

  def destroy
    @project.destroy
    redirect_to user_path(current_user)
  end

  private
    def verify_entity_user
      return if current_user.entity?

      redirect_to user_path(current_user)
    end

    def set_project
      @project = Project.find(params[:id])
    end

    def project_params
      params.require(:project).permit(:name, :description, :category)
    end
end
