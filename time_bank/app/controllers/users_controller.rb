# frozen_string_literal: true

class UsersController < ApplicationController
  def show
    @user = User.find(params[:id])
    @hours = @user.hours
  end

  def profile
    @user = User.find(params[:profile_id])
    @transaction = Transaction.new
    @message = Message.new
  end
end
