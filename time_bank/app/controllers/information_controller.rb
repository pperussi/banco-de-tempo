# frozen_string_literal: true

class InformationController < ApplicationController
  before_action :authenticate_user!
  before_action :set_information, only: [:edit, :update, :destroy]

  def index
    @information = Information.all
  end

  def new
    @information = Information.new
  end

  def create
    information = Information.new(permitted_params)

    if information.save
      flash[:alert] = "Cadastrada com sucesso"
      redirect_to admin_dashboard_path
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @information.update(permitted_params)
      redirect_to admin_dashboard_path
    else
      flash[:alert] = "Não foi possível atualizar notícia, tente novamente."
      redirect_to admin_dashboard_path
    end
  end

  def destroy
    if @information.destroy
      redirect_to admin_dashboard_path
    else
      flash[:alert] = "Não foi possível remover notícia, tente novamente."
      redirect_to admin_dashboard_path
    end
  end

  private
    def set_information
      @information = Information.find(params[:id])
    end

    def permitted_params
      params.require(:information).permit(:title, :text)
    end
end
