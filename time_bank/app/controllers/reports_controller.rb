# frozen_string_literal: true

class ReportsController < ApplicationController
  before_action :authenticate_user!
  before_action :search_transactions, only: [:index, :generate_csv]

  def index
    @monthly_hours = 0

    @transactions.each do |transaction|
      @monthly_hours += transaction.hours
    end
  end

  def generate_csv
    return empty_transactions_error if Transaction.count == 0

    respond_to do |format|
      format.html
      format.csv {
        send_data Transaction.to_csv(@transactions),
        filename: "banco-de-tempo-relatorio-mensal-#{DateTime.now.strftime("%d%m%Y%H%M")}.csv"
      }
    end
  end

  private
    def search_transactions
      @transactions = Transaction.approved.where(
        "created_at >= ? and created_at <= ?",
        Date.today.at_beginning_of_month,
        Date.today.at_end_of_month
      )
    end

    def empty_transactions_error
      flash[:alert] = "Nenhuma transação foi encontrada para gerar o relatório."
      redirect_to reports_path
    end
end
