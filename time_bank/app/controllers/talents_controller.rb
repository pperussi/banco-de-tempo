# frozen_string_literal: true

class TalentsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_talent, only: [:edit, :update, :destroy]

  def index
    @users = User.has_talent
  end

  def new
    @talent = Talent.new
  end

  def create
    @talent = Talent.new(talent_params)
    @talent.user = current_user

    if @talent.save
      flash[:notice] = "Cadastrado com sucesso."
      redirect_to user_path(current_user)
    else
      flash[:alert] = "Não foi possível cadastrar talento, tente novamente."
      render :new
    end
  end

  def edit
  end

  def update
    if @talent.update(talent_params)
      redirect_to user_path(current_user)
    else
      flash[:alert] = "Não foi possível atualizar talento, tente novamente."
      render :edit
    end
  end

  def destroy
    if @talent.destroy
      redirect_to user_path(current_user)
    else
      flash[:alert] = "Não foi possível remover talento, tente novamente."
      render :edit
    end
  end

  private
    def set_talent
      @talent = Talent.find(params[:id])
    end

    def talent_params
      params.require(:talent).permit(:name, :description, :category)
    end
end
