# frozen_string_literal: true

require "dry/monads"

class ExecuteTransaction
  include Dry::Monads[:result]

  def initialize(transaction)
    @user = transaction.provider
    @another_user = transaction.receiver
    @hours = transaction.hours
    @transaction = transaction
  end

  def call
    set_roles_in_transaction

    return Failure("O saldo do solicitante não é suficiente para a troca.") unless enough_balance?

    begin
      ActiveRecord::Base.transaction do
        provider.update(hours: updated_provider_balance)
        receiver.update(hours: updated_receiver_balance)

        transaction.update(status: :aprovada)
      end

    rescue StandardError
      return Failure("Não foi possível requisitar talento, tente novamente.")
    end

    Success(nil)
  end

  private
    attr_reader :user, :another_user, :receiver, :provider, :hours, :transaction

    def set_roles_in_transaction
      if another_user.entity?
        @provider = another_user
        @receiver = user
      else
        @provider = user
        @receiver = another_user
      end
    end

    def enough_balance?
      provider.hours > hours
    end

    def updated_provider_balance
      provider.hours - hours
    end

    def updated_receiver_balance
      receiver.hours + hours
    end
end
