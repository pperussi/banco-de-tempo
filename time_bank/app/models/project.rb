# frozen_string_literal: true

class Project < ApplicationRecord
  belongs_to :user

  enum category: [:arte_cultura, :direitos_humanos, :ambiental_ecológica, :questões_urbanas, :cidadania]

  validates :name, :description, :category, presence: true
end
