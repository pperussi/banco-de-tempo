# frozen_string_literal: true

class Information < ApplicationRecord
  validates :title, :text, presence: true
end
