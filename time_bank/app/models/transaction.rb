# frozen_string_literal: true

class Transaction < ApplicationRecord
  belongs_to :provider, class_name: "User"
  belongs_to :receiver, class_name: "User"

  enum status: [:pendente, :aprovada, :recusada, :cancelada]

  validates :hours, presence: true

  scope :approved, -> { where(status: "aprovada") }

  require "csv"

  def self.to_csv(transactions)
    column_names = ["Receptor", "Provedor", "Talento", "Horas", "Data"]

    CSV.generate do |csv|
      csv << column_names
      transactions.each do |transaction|
        # csv << transaction.attributes.values_at(*column_names)
        csv << [
          transaction.receiver.name,
          transaction.provider.name,
          transaction.talent,
          transaction.hours,
          transaction.created_at.strftime("%x %R")
        ]
      end
    end
  end
end
