# frozen_string_literal: true

class Talent < ApplicationRecord
  belongs_to :user

  enum category: [:ensino, :cuidado, :vida_domestica, :atividades_manuais]

  validates :name, :category, presence: true
end
