# frozen_string_literal: true

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :talents
  has_many :projects
  has_many :provider_transactions, class_name: "Transaction", foreign_key: "provider_id"
  has_many :receiver_transactions, class_name: "Transaction", foreign_key: "receiver_id"
  has_many :sender_messages, class_name: "Message", foreign_key: "sender_id"
  has_many :receiver_messages, class_name: "Message", foreign_key: "receiver_id"

  enum status: [:pendente, :ativo, :inativo]

  validates :name, presence: true

  scope :entities, -> { where(entity: true) }
  scope :not_entities, -> { where(entity: false) }
  scope :has_talent, -> { includes(:talents).where.not(talents: { id: nil }) }

  def entity?
    self.entity == true
  end

  def active?
    self.status == "ativo"
  end
end
