# frozen_string_literal: true

class Message < ApplicationRecord
  belongs_to :sender, class_name: "User"
  belongs_to :receiver, class_name: "User"

  validates :receiver, :message, presence: true
end
