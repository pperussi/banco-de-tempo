# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users

  root to: "initial_page#index"

  resources :users, only: %i[show]
  get "profile", to: "users#profile"

  resources :transactions, only: %i[index new create]
  delete "transactions/destroy", to: "transactions#destroy"
  put "transactions/deny", to: "transactions#deny"
  put "transactions/accept", to: "transactions#accept"

  resources :reports, only: %i[index]
  post "reports/search", to: "reports#search"
  post "reports/csv", to: "reports#generate_csv"

  resources :projects, only: %i[index new create edit update]
  get "projects/entity", to: "projects#entity"

  get "admin/dashboard", to: "admin#dashboard"
  get "admin/users", to: "admin#users"
  get "admin/entities", to: "admin#entities"
  get "admin/information", to: "admin#information"
  get "admin/bank_hours", to: "admin#bank_hours"

  put "admin/update_entity", to: "admin#update_entity"
  put "admin/update_user", to: "admin#update_user"

  post "admin/update_user", to: "admin#update_user"

  resources :talents
  resources :messages, only: %i[index new create]
  resources :information
end
