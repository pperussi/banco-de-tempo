class CreateProjects < ActiveRecord::Migration[7.0]
  def change
    create_table :projects do |t|
      t.references :user, null: false, type: :uuid, foreign_key: true
      t.string :name
      t.text :description
      t.integer :category

      t.timestamps
    end
  end
end
