class CreateTransactions < ActiveRecord::Migration[7.0]
  def change
    create_table :transactions do |t|
      t.references :receiver, null: false, type: :uuid
      t.references :provider, null: false, type: :uuid
      t.integer :hours
      t.string :talent
      t.string :project
      t.integer :status, default: 0

      t.timestamps
    end

    add_foreign_key :transactions, :users, column: :receiver_id
    add_foreign_key :transactions, :users, column: :provider_id
  end
end
