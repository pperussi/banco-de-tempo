class CreateUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :users, id: :uuid do |t|
      t.string :name
      t.text :description
      t.integer :hours, default: 4
      t.integer :status, default: 0
      t.boolean :entity, default: false
      t.boolean :admin, default: false

      t.timestamps
    end
  end
end
